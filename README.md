# Второй модуль

Создание страницы HelloWorld

---

На основании урока [devdocs.magento.com](https://devdocs.magento.com/videos/fundamentals/create-a-new-page/)

---

## Алгоритм создания модуля

### Шаг 1 : Создание директорий

- app/code/Vpv/PageHelloWorld = это модуль
- app/code/Vpv/PageHelloWorld/Controller/Page = место для экшена контроллера

### Шаг 2 : Создание файлов

- app/code/Vpv/registration.php = это файл регистрации модуля
- app/code/Vpv/FirstModule/etc/module.xml = это файл конфигурации модуля 
- app/code/Vpv/FirstModule/etc/frontend/routes.xml = файл маршрутизации
- app/code/Vpv/PageHelloWorld/Controller/Page/View.php = класс экшена 

### Шаг 3 : Проверка работоспособности модуля

Выполняем в консоли, в корне сайта, команду

- php bin/magento setup:upgrade

после выполнения которой, в консоль будут выводится все активированные модули, если новый модуль доступен - то он тоже выведется

Для уточняющей проверки всего вендора Vpv, можно выполнить консольную команду

- cat app/etc/config.php | grep Vpv_

которая, в случае положительного результата, отобразит вот это

-  'Vpv_FirstModule' => 1
-  'Vpv_PageHelloWorld' => 1

Заготовка второго модуля готова
